import socket
import sys

if len(sys.argv) < 2:
    print("Please enter the light dimming in % as the second argument")
    exit(1)

dimming = str(sys.argv[1])

msgFromClient       = "!000:LOUT=" + dimming + "\r\n"
bytesToSend         = str.encode(msgFromClient)
serverAddressPort   = ("192.168.1.83", 4001)
bufferSize          = 1024

# Create a UDP socket at client side
UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# Send to server using created UDP socket
UDPClientSocket.sendto(bytesToSend, serverAddressPort)

print(msgFromClient)

UDPClientSocket.close()

msgFromClient       = "!000:LOUT=" + dimming + "\r\n"
bytesToSend         = str.encode(msgFromClient)
serverAddressPort   = ("192.168.1.83", 4002)
bufferSize          = 1024

# Create a UDP socket at client side
UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# Send to server using created UDP socket
UDPClientSocket.sendto(bytesToSend, serverAddressPort)

print(msgFromClient)

UDPClientSocket.close()

#msgFromServer = UDPClientSocket.recvfrom(bufferSize)
#msg = "Message from Server {}".format(msgFromServer[0])

#print(msg)