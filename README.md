# Scripts to use with ML tracking software on the tensorbook

## Starting up the latest setup (R/V Fulmar Oct 2020)

With the integration of the ML software and MiniROV control software we now can spin up
all of the ML processes from procman-sheriff via:

```
ops@tensorbook1:~$ gosoft
ops@tensorbook1:~/prj.local/BioEng$ cd scripts
ops@tensorbook1:~/prj.local/BioEng/scripts$ ./run_control_fulmar
```

## De-Scription



### bigUDPBuffers

This script increases the UDP buffer size and is required for any LCM comms that send images

### log20GbLcmFiles

This script starts the lcm-logger with a channel list selected to save only the critical LCM channels for reproducing a dive. The files are broken up into 20 GBs

### run_box_sync

This runs a python process to listen for BOX_LEFT and BOX_RIGHT messages, synchronizes the
messages, and publishes BOX_STEREO message with the box lists from each channel

### run_boxview

This runs the glue that holds the system together to ingest synchronized lcm messages and
output target positions in 2D or 3D.

### run_control_fulmar

This script runs the latest deployment configuration for the Oct. 2020 Fulmar cruise. It points to the deployment in ```~/prj.local/BioEng/mwt-control/deployments/minirov.oct.23.2020```

### run_cvisionai_tracker

This runs the stereo tracker algorithm from CVisionAI. Make sure that before you run this script you have set the correct tuning parameters and stereo calibration file in ```~/prj.local/BioEng/mbari_tracking/track/track.py```

### run_designer

This runs QtDesigner 

### run_image_sync

This is a legacy script that is only needed when playing back lcm log files to join MANTA_LEFT and MANTA_RIGHT images together and publish MWT_STEREO_IMAGE messages

### run_mantaview

This script will start the mantaview app that allows control of cameras and lights

### run_ml_low_latency

This runs a docker container with the ML models. Open the script to check arguments and defaults. This file should be kept up to date with the latest model tag from cvisionai's docker hub. The current tag is 2020.08.11

### run_pycharm

This runs pycharm

### runspy.sh

This is a symlink to run LCM Spy with the jar file created from mwt-lcm-type repo

### run_stereo_sync

This run a python process to listen for MWT_STEREO_IMAGE messages, and BOX_STEREO messages, synchronizes them, and publishes a STEREO_IMAGE_WITH_BOXES message

### run_vimcam

This runs the camera acquisition software

### stop_containers

This will stop all docker containers running on the tensorbook

### VimbaViewer

This will start the VimbaViewer application



## Notes

For some reason with you don't unset PYTHONPATH any script called from procman will use the
system python2.7. Just add the following to your script to use the virtualenv python

```
unset PYTHONPATH
```

